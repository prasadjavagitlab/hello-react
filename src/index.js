import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';
// import { books } from './books';
// import Book from './Book';

import UseStateBasics from './UseStateBasics';

// const BookList = () => {
//   return (
//     <section>
//       {books.map((book, index) => {
//         return <Book key={index} {...book}></Book>;
//       })}
//     </section>
//   );
// };

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<UseStateBasics />);
