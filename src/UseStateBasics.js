import React, { useState } from 'react';
import { data } from './data';

const UseStateBasics = () => {
  const [people, setPeople] = useState(data);
  const removeItem = i => {
    let newPeople = people.filter(person => person.id !== i);
    setPeople(newPeople);
  };
  return (
    <>
      {people.map(item => {
        const { id, name } = item;
        return (
          <div key={id} className="row">
            <h4 className="col-md-2">{name}</h4>
            <button
              className="col-md-1 col-xs-2"
              onClick={() => removeItem(id)}
            >
              remove
            </button>
          </div>
        );
      })}
      <button className="btn btn-primary" onClick={() => setPeople([])}>
        clear items
      </button>
    </>
  );
};

export default UseStateBasics;
