function Person(name, age) {
  this.name = name;
  this.age = age;
}
Person.prototype.sayName = function (x) {
  console.log(` ${x}, ${this.name} `);
};
var p1 = new Person('Prasad', 38);
var sname = p1.sayName;
sname.call(p1, 'hai');
sname.apply(p1, ['hello']);
var boundSName = sname.bind(p1, 'hi');
boundSName();
