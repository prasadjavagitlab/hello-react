import React from 'react';
class App extends React.Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   count: 25,
    // };

    // this.increment = this.increment.bind(this);

    // this.decrement = this.decrement.bind(this);

    console.log(this);
  }
  state = {
    count: 25,
  };
  // increment() {
  //   this.setState({
  //     count: this.state.count + 1,
  //   });
  // }
  increment = e => {
    this.setState({
      count: this.state.count + 1,
    });
  };
  decrement() {
    this.setState({
      count: this.state.count - 1,
    });
  }
  render() {
    console.log(this.state);
    return (
      <div>
        {/* <h2>Counter</h2>
        <div>
          <button onClick={e => this.decrement(e)}>-</button>
          <span>{this.state.count}</span>
          <button onClick={this.increment}>+</button>
        </div> */}
      </div>
    );
  }
}

export default App;
